$(document).ready(function(){

  $(".owl-carousel").owlCarousel({
    margin: 5,
    lazyLoad: true,
    responsive : {
      0: {
        items: 1,
      },
      992 : {
        items: 3,
      },
    }
  });

  $(".layout__gallery").owlCarousel({
    margin: 5,
    lazyLoad: true,
    responsive: {
      0: {
        items: 1,
      },
      480: {
        items: 2,
      },
      768: {
        items: 3,
      }
    }
  });

  $("a.fancybox").fancybox({});
});


var hamburger = document.querySelector('.hamburger');

hamburger.onclick = function(e) {
  e.preventDefault();
  hamburger.classList.toggle('is-active');
  document.getElementById('main-nav').classList.toggle('main-nav--active');
};